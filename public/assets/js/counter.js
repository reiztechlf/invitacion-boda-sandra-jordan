// Fecha objetivo para la cuenta regresiva
var countDownDate = new Date("Nov 30, 2024 14:00:00").getTime();

// Actualiza la cuenta regresiva cada 1 segundo
var x = setInterval(function() {

    // Obtén la fecha y hora actual
    var now = new Date().getTime();

    // Calcula la diferencia entre la fecha objetivo y la actual
    var distance = countDownDate - now;

    // Calcula los días, horas, minutos y segundos
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Muestra los resultados en los elementos respectivos
    document.getElementById("days").innerHTML = days;
    document.getElementById("hours").innerHTML = hours;
    document.getElementById("minutes").innerHTML = minutes;
    document.getElementById("seconds").innerHTML = seconds;

    // Si la cuenta regresiva ha terminado, muestra un mensaje
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
    }
}, 1000);
